# Ejercicio Javascript Colvin

Ejercicio Javascript del test Colvin.
Microservicio para contar los nombres que incluyen las clases de un repositorio.

## Primeros pasos

### Requisitos previos

* [npm](https://www.npmjs.com/)
* [Node.js](https://nodejs.org/)

### Instalación

Instalar dependencias del proyecto

`npm install`

Ejecutar servidor de desarrollo en el puerto 3000

`npm run dev`

## Despliegue

Ejecutar el servidor en producción

`npm run prod`

## Pruebas

Ejecutar las pruebas

`npm run test`

## Token GitHub
El repositorio contiene un token de GitHub a proposito, no tiene ningún permiso de escritura pero se incluye para simplificar la instalación.