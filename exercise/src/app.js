'use strict';

const bodyParser = require('body-parser');

const actions = require('./actions');
const classCounter = actions.classCounter;

module.exports = (app, express) => {
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    
    app.get('/ping', (req, res) => res.status(200).send('pong'));

    app.get('/', (req, res) => {
        const {
            author,
            repository
        } = req.query;

        if(!author || !repository) {
            res.status(200).send({
                data: {
                    author: '<required> Author',
                    repository: '<required> Repository'
                }
            });    
        }

        classCounter(author, repository, (err, result) => {
            if(err) res.status(500).send(err);
            else {
                res.status(200).send(result);
            }
        });
    });

    return app;
};