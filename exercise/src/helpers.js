const splitWord = (word) => {
    const regex = /([a-z](?=[A-Z]))/g;
    return word.replace(regex, '$1 ').split(' ');
};

const removeExtension = (name) => {
    return name.split('.')[0];
};

const isCamelCase = (word) => {
    return word !== word.toLowerCase() 
    && word !== word.toUpperCase()
    && !word.includes('-')
    && !word.includes('_');
};

module.exports = {
    splitWord,
    removeExtension,
    isCamelCase
};