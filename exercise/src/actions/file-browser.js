'use strict';

const GitHubApi = require('github');
const async = require('async');

const helpers = require('../helpers');

const fileBrowser = (github) => (author, repository, path, cb) => {
    const getElements = (author, repository, path, callback) => {
        github.repos.getContent({
            owner: author,
            repo: repository,
            path
        }, (err, result) => {
            if(err) callback({ githubError: err });
            else callback(null, result.data)
        });
    };

    const inspectEachElement = (elements, callback) => {
        const wordsCount = [];

        async.each(elements, (element, next) => {
            if(element.type === 'file') {
                const nameWithoutExtension = helpers.removeExtension(element.name);
                if(helpers.isCamelCase(nameWithoutExtension)) {
                    const wordSplitted = helpers.splitWord(nameWithoutExtension);
                    async.each(wordSplitted, (word, done) => {
                        const existingWord = wordsCount.findIndex(obj => obj.name === word);
                        if(existingWord > -1) {
                            wordsCount[existingWord].count++;
                        } else {
                            const newObject = { name: word, count: 1 };
                            wordsCount.push(newObject);
                        }
                        done();
                    }, next);
                } else next();
            } else if(element.type === 'dir') {
                fileBrowser(github)(author, repository, element.path, (err, result) => {
                    async.each(result, (wordObject, done) => {
                        const existingWord = wordsCount.findIndex(obj => obj.name === wordObject.name);
                        if(existingWord > -1) {
                            wordsCount[existingWord].count += wordObject.count;
                        } else {
                            const newObject = { name: wordObject.name, count: wordObject.count };
                            wordsCount.push(newObject);
                        }
                        done();
                    });
                    next();
                });
            }
        }, (err, result) => {
            callback(null, wordsCount);
        });
    };

    async.waterfall([
        next => getElements(author, repository, path, next),
        (elements, next) => inspectEachElement(elements, next)
    ], cb);
};

module.exports = fileBrowser;