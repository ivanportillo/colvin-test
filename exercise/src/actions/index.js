const GitHubApi = require('github');
const github = new GitHubApi({});

const createClassCounter = require('./class-counter');
const createFileBrowser = require('./file-browser');

github.authenticate({
    type: 'token',
    token: '3f0390ea04a5caa285ddb3d299950b7bd150eb9e'
});

module.exports = {
    classCounter: createClassCounter(github),
    fileBrowser: createFileBrowser(github)
};