'use strict';

const async = require('async');

const fileBrowser = require('./file-browser');

module.exports = (github) => (author, repository, cb) => {
    const browseFiles = (author, repository, callback) => {
        fileBrowser(github)(author, repository, '', callback);
    };

    const sortCount = (count, callback) => {
        count.sort((a, b) => (a.count > b.count) ? -1 : ((b.count > a.count) ? 1 : 0));
        callback(null, count);
    };

    const convertToObject = (countArray, callback) => {
        let countObject = {};
        async.each(countArray, (element, next) => {
            countObject = {
                ...countObject,
                [element.name]: element.count
            };
            next();
        }, () => callback(null, countObject));
    };

    async.waterfall([
        next => browseFiles(author, repository, next),
        (count, next) => sortCount(count, next),
        (count, next) => convertToObject(count, next)
    ], cb);
};