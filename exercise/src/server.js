'use strict';

const express = require('express');
const app = express();

const listenPort = process.env.PORT || 3000;

require('./app')(app, express);

app.listen(listenPort, () => {
    console.log(`Server listening at port ${listenPort}`);
});