const helpers = require('../../src/helpers');

describe('Helpers', () => {
    describe('Word splitter', () => {
        it('Should return an array with 3 strings given MyTestClass', () => {
            const word = 'MyTestClass';
            const wordSplitted = ['My', 'Test', 'Class'];
            const result = helpers.splitWord(word);
            expect(result).toHaveLength(3);
            expect(result).toEqual(wordSplitted);
        });

        it('Should return an array with 2 strings given myClass', () => {
            const word = 'myClass';
            const wordSplitted = ['my', 'Class'];
            const result = helpers.splitWord(word);
            expect(result).toHaveLength(2);
            expect(result).toEqual(wordSplitted);
        });

        it('Should return an array with 1 strings given Emitter', () => {
            const word = 'Emitter';
            const wordSplitted = ['Emitter'];
            const result = helpers.splitWord(word);
            expect(result).toHaveLength(1);
            expect(result).toEqual(wordSplitted);
        });

        it('Should return an array with 1 strings given emitter', () => {
            const word = 'emitter';
            const wordSplitted = ['emitter'];
            const result = helpers.splitWord(word);
            expect(result).toHaveLength(1);
            expect(result).toEqual(wordSplitted);
        });

        it('Should return an array with 2 strings given UtilsDOM', () => {
            const word = 'UtilsDOM';
            const wordSplitted = ['Utils', 'DOM'];
            const result = helpers.splitWord(word);
            expect(result).toHaveLength(2);
            expect(result).toEqual(wordSplitted);
        });
    });

    describe('Extension remover', () => {
        it('Should return photo given photo.jpg (one dot)', () => {
            const filename = 'photo.jpg';
            const nameWithoutExtension = 'photo';
            const result = helpers.removeExtension(filename);
            expect(result).toEqual(nameWithoutExtension);
        });

        it('Should return myComponent given myComponent.d.ts (two dots)', () => {
            const filename = 'myComponent.d.ts';
            const nameWithoutExtension = 'myComponent';
            const result = helpers.removeExtension(filename);
            expect(result).toEqual(nameWithoutExtension);
        });
    });

    describe('Camel case checker', () => {
        it('Should return true given CamelCase', () => {
            const word = 'CamelCase';
            const result = helpers.isCamelCase(word);
            expect(result).toBeTruthy();
        });

        it('Should return true given Camelcase', () => {
            const word = 'Camelcase';
            const result = helpers.isCamelCase(word);
            expect(result).toBeTruthy();
        });
        
        it('Should return true given camelCase', () => {
            const word = 'camelCase';
            const result = helpers.isCamelCase(word);
            expect(result).toBeTruthy();
        });

        it('Should return false given camelcase', () => {
            const word = 'camelcase';
            const result = helpers.isCamelCase(word);
            expect(result).toBeFalsy();
        });

        it('Should return false given camelCase-camel', () => {
            const word = 'camelCase-camel';
            const result = helpers.isCamelCase(word);
            expect(result).toBeFalsy();
        });
    });
});