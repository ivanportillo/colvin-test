const fakeAuthor = 'GuyIncognito';
const fakeRepository = 'BestRepository';

const rootResponse = [
    { 
        name: 'readme.md',
        path: 'readme.md',
        sha: 'bba2a4c5c52ba756898193bcbc4f86e269cc693d',
        size: 20839,
        url: 'url',
        html_url: 'html_url',
        git_url: 'git_url',
        download_url: 'download_url',
        type: 'file'
    }, {
        name: 'class',
        path: 'class',
        sha: '1fff8ad5d03d5cbeef412ddb1ffbe05984db5187',
        size: 0,
        url: 'url',
        html_url: 'html_url',
        git_url: 'git_url',
        download_url: 'download_url',
        type: 'dir'
    }
];

const classResponse = [
    {
        name: 'MyPerfectClass.js',
        path: 'class/MyPerfectClass.js',
        sha: 'bba2a4c5c52ba756898193bcbc4f86e269cc693d',
        size: 20839,
        url: 'url',
        html_url: 'html_url',
        git_url: 'git_url',
        download_url: 'download_url',
        type: 'file'
    }, {
        name: 'MyNotReallyGoodClass.js',
        path: 'class/MyNotReallyGoodClass.js',
        sha: 'bba2a4c5c52ba756898193bcbc4f86e269cc693d',
        size: 20839,
        url: 'url',
        html_url: 'html_url',
        git_url: 'git_url',
        download_url: 'download_url',
        type: 'file'
    }, {
        name: 'RefactorASAPClass.js',
        path: 'class/RefactorASAPClass.js',
        sha: 'bba2a4c5c52ba756898193bcbc4f86e269cc693d',
        size: 20839,
        url: 'url',
        html_url: 'html_url',
        git_url: 'git_url',
        download_url: 'download_url',
        type: 'file'
    }, {
        name: 'DontKnowWhyItIsWorkingClass.js',
        path: 'class/DontKnowWhyItIsWorkingClass.js',
        sha: 'bba2a4c5c52ba756898193bcbc4f86e269cc693d',
        size: 20839,
        url: 'url',
        html_url: 'html_url',
        git_url: 'git_url',
        download_url: 'download_url',
        type: 'file'
    }
]

const notFoundError = {
    "code": 404,
    "status": "Not Found",
    "message": "{\"message\":\"Not Found\",\"documentation_url\":\"https://developer.github.com/v3\"}"
};

const mockedGithub = {
    repos: {
        getContent: ({ owner, repo, path }, cb) => {
            if(owner === fakeAuthor && repo === fakeRepository) {
                if(path === '') cb(null, { data: rootResponse });
                else if(path === 'class') cb(null, { data: classResponse });
            } else {
                cb(notFoundError);
            }
        }
    }
};

const fileBrowser = require('../../../src/actions/file-browser')(mockedGithub);

describe('File browser action', () => {
    const countResult = [ 
        { name: 'My', count: 2 },
        { name: 'Perfect', count: 1 },
        { name: 'Class', count: 3 },
        { name: 'Not', count: 1 },
        { name: 'Really', count: 1 },
        { name: 'Good', count: 1 },
        { name: 'Refactor', count: 1 },
        { name: 'ASAPClass', count: 1 },
        { name: 'Dont', count: 1 },
        { name: 'Know', count: 1 },
        { name: 'Why', count: 1 },
        { name: 'It', count: 1 },
        { name: 'Is', count: 1 },
        { name: 'Working', count: 1 } 
    ];

    it('Should return an array with counts unsorted', done => {
        const rootPath = '';
        fileBrowser(fakeAuthor, fakeRepository, rootPath, (err, result) => {
            expect(result).toEqual(countResult);
            done();
        });
    });

    it('Should return a github error given a nonexistent repository or user', done => {
        const rootPath = '';
        const badRepository = 'badRepository';
        fileBrowser(fakeAuthor, badRepository, rootPath, (err, result) => {
            expect(err).toEqual({ githubError: notFoundError });
            done();
        });
    });
});