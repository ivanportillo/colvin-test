# Test

**Describe varias de las herramientas de desarrollo que te guste utilizar y explica por qué.**
Mis herramientas de desarrollo favoritas son las siguientes:

- Editor: Visual Studio Code, prefiero un editor frente a un IDE porque puedo tener exactamente todo lo que necesito y no muchas funcionalidades que no voy a utilizar. Y en concreto, Visual Code me gusta por su sencillez y su barra lateral con acceso rápido a las principales funcionalidades.

- Cliente Rest: Insomnia, un simple cliente para hacer peticiones a cualquier API, incluso endpoints GraphQL.

- Terminal: iTerm, terminal customizable con bastantes atajos de teclado.

- Herramienta de mailing: Mailchimp, es la única que utilizado, tanto interfaz como API.

- Plataforma de desarrollo: GitHub, y, aunque también utilizo GitLab, mi principal plataforma es GitHub.

- Herramienta de comunicación/colaboración: Slack.

**¿Qué conocimientos has aprendido por tu cuenta en el último año? ¿Cuales te gustaría adquirir en el próximo año?**
El pasado año 2017 he aprendido una serie de conocimientos: 
- En Frontend:
    - He profundizado mis conocimientos sobre testing además de la herramienta Jest
    - He añadido tipado a Javascript haciendo uso de Typescript
    - He mejorado el bundling de mis apps con Webpack.
    - He mejorado el styling y favorecido la reutilización de componentes haciendo uso de Styled-Components.
    - He mejorado la arquitectura de mis apps React haciendo uso de Redux-Saga para las acciones asíncronas.
    - He solucionado la trivial y repetitiva tarea de los formularios con Redux-Form.
    - He aprendido más sobre patrones en React, como el uso de HOCs y programación funcional. Disminuyendo el acoplamiento y favoreciendo de nuevo la reutilización.
    - He utilizado GraphQL en proyectos personales.
    - He utilizado WebSocket en React Native para comunicación en tiempo real.

- En Backend:
    - He hecho uso de patrones de diseño como Inyección de dependencias y Patrón repositorio.
    - He testeado una aplicación con el proceso BDD, con las pruebas escritas con lenguaje que puede entender el cliente.
    - He implementado un simple sistema de reglas para realizar diagnosis.
    - He profundizado en la programación dirigida por eventos para disparar alarmas.
    - He mejorado mi control sobre la programación asíncrona.
    - He utilizado una cola de mensajes como RabbitMQ para la comunicación entre dispositivos y el backend.
    - He leido algo sobre DDD, CQRS y Event Sourcing (demasiado poco, por ahora 😅).


El presente año 2018 me gustaría aprender:
- A pesar de manejar bien la inmutabilidad del estado, me gustaría hacer uso de librerías que eviten errores.
- Mejorar en los procesos de desarrollo y reducir los errores, sobretodo mejorar en estimar tareas.
- Aprender más sobre CI/CD, me encanta la idea de poder automatizar las tareas repetitivas.
- Automatizar con una mini-librería la repetitiva tarea de hacer fetching con Redux Saga.
- Aprender DDD, CQRS y Event Sourcing 🙂
- Utilizar MQTT en IOT, para un proyecto personal.
    
En definitiva, todo lo que pueda aprender y mejore mis habilidades.

**¿Qué sabes de patrones de diseño? ¿Cuales sueles usar y en qué situaciones?**
Los patrones de diseño son soluciones reutilizables y ya probadas a problemas de diseño. Hago uso de los patrones de diseño tanto, para problemas que he tenido que resolver más de una vez como para aquellos nuevos que se me presentan. Algunos de los que más suelo utilizar son los siguientes:

- Patrón repositorio: el acceso a base de datos es recomendable hacerlo contra un repositorio y no directamente contra el driver / ORM de la base de datos, de esta manera, sería sencillo cambiar este acceso, incluso a otra base de datos si fuera necesario.

- Inyección de dependencias: este patrón consiste en inyectar las dependencias y así, desacoplar nuestro código de las mismas. He utilizado este patrón principalmente en backend, sobretodo para inyectar repositorios, servicios y demás que vaya a utilizar.

- Singleton: clases que solo permiten una única instancia. Bastante útil para realizar módulos en NodeJS o para elementos que debamos tener la misma instancia en toda una suite de tests, por ejemplo.

- Flux (Redux): al utilizar Redux junto React hago uso de una especie de Flux, que me ayuda a mantener el estado de mi aplicación de una manera muy sencilla y totalmente controlada.

- MVC: en el backend de las aplicaciones que hago utilizando NodeJS, hago uso de este patrón separando la aplicación en Modelo, Vista y Controlador.

**¿Cómo planificarías la internacionalización de una app? ¿Qué tareas y dificultades puedes prever en el desarrollo, testing y lanzamiento?**
En la internacionalización de una app, los principales problemas que veo son legislación y cultura diferente. Hay que cumplir adecuadamente la legislación del país dónde vamos a extendernos sobre todo si se manejan pagos, y por otra parte, hay que hacer un estudio del mercado ya que los usuarios de cada país tienen unas necesidades y una forma de entender la aplicación diferente.

Las posibles tareas y dificultades que puedo prever:

- Desarrollo: una de las tareas en desarrollo sería la traducción pero realmente no es una dificultad ya que no es muy complicado mostrar un mensaje en un lenguaje u otro dependiendo del lenguaje del sistema. Realmente, el problema lo veo en adaptar la aplicación a la legislación, por ejemplo, niveles de seguridad tratando datos bancarios según en qué país.

- Testing y lanzamiento: una tarea complicada podría ser la planificación para el lanzamiento de diferentes features en diferentes mercados. Por ejemplo, lanzar primero una feature en un mercado para probarlo de manera aislada.


**¿Qué funcionalidades debería tener la app de Colvin que no tiene la web?**
Las funcionalidades que considero que debería tener la app y que no tiene la web son:

- Pago rápido: al poder utilizar la app desde el móvil en cualquier lugar y cualquier momento, podría ser interesante almacenar la información del usuario de compras anteriores o en el registro para que pueda hacer una compra en los menores pasos posibles. Esta funcionalidad también sería de utilidad en la web pero sobre todo en la app móvil.

- Seguimiento: tras hacer un pedido, poder ver el seguimiento de la manera más atractiva y simple posible.

- Notificaciones: haciendo uso de las notificaciones push, recibir de manera periódica descuentos, tanto personalizados para cada usuario a modo de bonificación como generales como rebajas y demás.

- Chat: establecer un chat como medio de comunicación tanto para soporte como dudas y sugerencias.

**¿Qué tres apps del App Store/Google Play te gustan más a nivel UX? Por qué? ¿Qué mejorarías a nivel UX de la app Cheerz (App Store / Google Play)?**
A nivel UX, las tres aplicaciones que mas me gustan de la App Store son:

- Revolut: esta app hace nos permite de manera muy sencilla poder ver todas las transacciones que has realizado en cualquier moneda que manejes. Además, de un proceso muy simplificado para enviar/recibir dinero o controlar una tarjeta de débito (bloquearla, activar solo algunas caracteristicas...).

- Path: a pesar de no ser un usuario activo de esta red social, si que llevo unos años siguiendo esta aplicación precisamente por su interfaz. Especialmente, por su simple interfaz y su multitud de pequeños y cuidados detalles sin llegar a estar sobrecargada.

- Airbnb: me llama la atención esta aplicación por la manera que muestran tanta información (cada alojamiento debe tener mucha info) de una manera muy simple y visualmente atractiva.

Sobre la app Cheerz, el aspecto que mejoraría sería la navegación en los productos que ofrecen. Cuando accedes a un producto y te lleva a un segundo nivel (por ejemplo, albums), la navegación es la misma y no da la sensación de estar en ese segundo nivel. Esto podría mejorarse con una forma diferente de mostrar los elementos al pasar a un segundo nivel, como un grid con cada elemento más pequeño.

# Ejercicio
Instrucciones en la carpeta [exercise](https://gitlab.com/ivanportillo/colvin-test/tree/master/exercise)